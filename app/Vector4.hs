{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
module Vector4 where

import Prelude hiding(length)
import Control.Comonad(Comonad,extract,duplicate)
import Control.Monad(liftM2)
import Utils.Compose
import Utils.Tuple

data Vector4 a = Vector4 a a a a deriving (Show,Eq,Ord)

instance Functor Vector4 where
    fmap f (Vector4 x y z w) = Vector4 (f x) (f y) (f z) (f w)

instance Applicative Vector4 where
    pure a = Vector4 a a a a
    liftA2 f (Vector4 ax ay az aw) (Vector4 bx by bz bw) = 
            Vector4 (f ax bx) (f ay by) (f az bz) (f aw bw)

v4x, v4y, v4z, v4w :: Vector4 a -> a
v4x (Vector4 x _ _ _) = x
v4y (Vector4 _ y _ _) = y
v4z (Vector4 _ _ z _) = z
v4w (Vector4 _ _ _ w) = w

instance Monad Vector4 where
    (Vector4 x y z w) >>= f = Vector4 (v4x.f $ x) (v4y.f $ y) (v4z.f $ z) (v4w.f $ w)

instance Comonad Vector4 where
    extract = v4x
    duplicate (Vector4 x y z w) = Vector4 (Vector4 x y z w) (Vector4 y z w x) (Vector4 z w x y) (Vector4 w x y z)

instance Foldable Vector4 where
    foldr f b (Vector4 x y z w) = (f x . f y . f z . f w) b

-- eww
toList :: Vector4 a -> [a]
toList (Vector4 x y z w) = [x,y,z,w]
fromList :: [a] -> Vector4 a
fromList [x,y,z,w] = (Vector4 x y z w)
fromList _ = error "non 4 list trying to become a Vector4"
instance Traversable Vector4 where
    sequenceA = fmap fromList . sequenceA . toList

instance Num a => Num (Vector4 a) where
    (+) = liftA2 (+)
    (-) = liftA2 (-)
    (*) = liftA2 (*)
    abs = fmap abs
    signum = fmap signum
    fromInteger = pure . fromInteger

vsum :: Num a => Vector4 a -> a
vsum = foldr (+) 0
vproduct :: Num a => Vector4 a -> a
vproduct = foldr (*) 0

zero, x, y, z, w :: Num a => Vector4 a
zero = 0
x = Vector4 1 0 0 0
y = Vector4 0 1 0 0
z = Vector4 0 0 1 0
w = Vector4 0 0 0 1

length² :: Num a => Vector4 a -> a
length² = vsum . fmap (^2)
length :: Floating a => Vector4 a -> a
length = sqrt . length²

dist² :: Num a => Vector4 a -> Vector4 a -> a
dist² = length² .#. (-)
dist :: Floating a => Vector4 a -> Vector4 a -> a
dist = length .#. (-)

dot :: Num a => Vector4 a -> Vector4 a -> a
dot = vsum .#. (*)

cross :: Num a => Vector4 a -> Vector4 a -> Vector4 a
cross (Vector4 ax ay az aw) (Vector4 bx by bz bw)
        = Vector4  (ay*bz-az*by) (az*bx-ax*bz) (ax*by-ay*bx) (aw*bw)

scale :: Num a => a -> Vector4 a -> Vector4 a
scale = fmap . (*)

normalise :: Floating a => Vector4 a -> Vector4 a
normalise v = scale (recip.length $ v) v

inverse :: Floating a => Vector4 a -> Vector4 a
inverse v = scale (recip.length² $ v) v

perpendicular3D :: (Eq a, Num a) => Vector4 a -> (Vector4 a, Vector4 a)
perpendicular3D v@(Vector4 x y z w) = (u,s)
                where 
                    sign x = if x == 0 then -1 else signum x
                    copysign a b = sign b * abs a
                    u = Vector4 (copysign z x) (copysign z y) (negate $ copysign (abs x +  abs y) z) w
                    s = cross u v

perpendicular3Dl :: (Eq a, Num a) => Vector4 a -> [Vector4 a]
perpendicular3Dl = listify . perpendicular3D

project :: Floating a => Vector4 a -> Vector4 a -> Vector4 a
project v = scale =<< liftM2 (/) (dot v) length²

spanProject :: Floating a => Vector4 a -> [Vector4 a] -> Vector4 a
spanProject = sum .#. fmap . project

todo :: a
todo = error "TODO"
