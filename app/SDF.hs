module SDF where

import Control.Comonad(Comonad,extract,duplicate)
import Utils.Arithmetic
import qualified Vector4 as V

data SDF a = SDF { getSDF :: V.Vector4 Double -> a }

instance Functor SDF where
    fmap f = SDF . fmap f . getSDF

instance Applicative SDF where
    pure = SDF . const
    liftA2 f sdf1 sdf2 = SDF $ \p -> f (getSDF sdf1 p) (getSDF sdf2 p)

instance Monad SDF where
    sdf >>= gen = SDF $ \p -> getSDF (gen $ getSDF sdf p) p

instance Comonad SDF where
    extract sdf = getSDF sdf (V.Vector4 0 0 0 0)
    duplicate sdf = SDF $ \p -> SDF $ \q -> getSDF sdf (q-p)

instance (Ord a, Num a) => Num (SDF a) where
    (+) = liftA2 min
    (*) = liftA2 max
    negate = fmap negate
    abs = fmap abs
    signum = fmap signum
    fromInteger = pure . fromInteger

-----
point :: V.Vector4 Double -> SDF Double
point = SDF . V.dist

line :: V.Vector4 Double -> V.Vector4 Double -> SDF Double
line a b = SDF $ \p -> let t = clamp 0 1 $ V.dot (p-a) (b-a)/V.dist² b a
                        in V.dist p $ a + V.scale t (b-a)

fatten :: Num a => a -> SDF a -> SDF a
fatten = fmap . (+) . negate
{--
line a b = SDF $ \p -> case V.dot (p-a) (b-a)/V.dist² b a of
                        t | t > 1       -> V.dist p b
                        t | t < 0       -> V.dist p a
                        t | otherwise   -> V.dist p $ a + V.scale t (b-a) --move along line perpendicular
--}

