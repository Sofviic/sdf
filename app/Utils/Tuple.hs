module Utils.Tuple where

import Control.Monad(join)
import Control.Arrow((***))

uncurry4 :: (a -> b -> c -> d -> e) -> (a,b,c,d) -> e
uncurry4 f (a,b,c,d) = f a b c d
uncurry3 :: (a -> b -> c -> d) -> (a,b,c) -> d
uncurry3 f (a,b,c) = f a b c

map4a :: (a -> b) -> (a,a,a,a) -> (b,b,b,b)
map4a f (a,b,c,d) = (f a,f b,f c,f d)

sequence4 :: [(a,b,c,d)] -> ([a],[b],[c],[d])
sequence4 x = (fmap fst4 x, fmap snd4 x, fmap trd4 x, fmap fth4 x)

fst4 :: (a,b,c,d) -> a
fst4 (x,_,_,_) = x
snd4 :: (a,b,c,d) -> b
snd4 (_,x,_,_) = x
trd4 :: (a,b,c,d) -> c
trd4 (_,_,x,_) = x
fth4 :: (a,b,c,d) -> d
fth4 (_,_,_,x) = x

listify :: (a,a) -> [a]
listify (a,b) = [a,b]

both :: (a -> b) -> (a,a) -> (b,b)
both = join (***)
