module Utils.List where

fmaps :: [a -> b] -> a -> [b]
fmaps fs = flip fmap fs . flip ($)
