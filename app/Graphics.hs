module Graphics where

import qualified SDF
import qualified Vector4 as V
import GHC.Float
import Utils.Tuple

type Colour = (Int,Int,Int)
type FOV = (Double,Double)

screendim :: (Int,Int)
screendim = (192,108)

screendimfactor :: Int
screendimfactor = 10

screendist :: Double
screendist = 10

colour :: Colour
colour = (100,0,200)

bg :: Colour
bg = (0,0,0)

far_limit :: Double
far_limit = 1000

prc_limit :: Double
prc_limit = 1e-1

step_limit :: Int
step_limit = 10

screenloc :: V.Vector4 Double -> V.Vector4 Double -> V.Vector4 Double
screenloc cam_loc cam_facing = cam_loc + V.scale screendist cam_facing

screenpixloc :: V.Vector4 Double -> V.Vector4 Double -> FOV -> Int -> Int -> V.Vector4 Double
screenpixloc cam_loc cam_facing (hfov,vfov) i j = screenWorldLoc + xWorldOffset + yWorldOffset
                            where
                                (x,y) = (int2Double i, int2Double j)
                                screendimD = both int2Double screendim
                                screenWorldRadX = screendist * tan (hfov / 2)
                                screenWorldRadY = screendist * tan (vfov / 2)
                                screenWorldLoc = cam_loc + V.scale screendist cam_facing
                                screenWorldUpX = V.normalise $ V.cross screenWorldUpY cam_facing
                                screenWorldUpY = V.normalise . V.spanProject V.y . V.perpendicular3Dl $ cam_facing
                                xWorldDist = (2*x / fst screendimD - 1) * screenWorldRadX
                                yWorldDist = (2*y / snd screendimD - 1) * screenWorldRadY
                                xWorldOffset = V.scale xWorldDist screenWorldUpX
                                yWorldOffset = V.scale yWorldDist screenWorldUpY

displayPixel :: SDF.SDF Double -> V.Vector4 Double -> V.Vector4 Double -> FOV -> Int -> Int -> Colour
displayPixel sdf cam_loc cam_facing (hfov,vfov) i j = raymarch sdf pix_loc ray
                                                    where 
                                                        pix_loc = screenpixloc cam_loc cam_facing (hfov,vfov) i j
                                                        ray = V.normalise $ pix_loc - cam_loc

raymarch' :: Int -> SDF.SDF Double -> V.Vector4 Double -> V.Vector4 Double -> Colour
raymarch' indx sdf loc unit_ray | SDF.getSDF sdf loc < prc_limit = colour
                                | SDF.getSDF sdf loc > far_limit = bg
                                | isNaN $ SDF.getSDF sdf loc     = bg
                                | indx > step_limit              = bg
                                | otherwise                      = raymarch' (indx+1) sdf (loc + V.scale (SDF.getSDF sdf loc) unit_ray) unit_ray

raymarch :: SDF.SDF Double -> V.Vector4 Double -> V.Vector4 Double -> Colour
raymarch = raymarch' 0

todo :: a
todo = error "TODO"
