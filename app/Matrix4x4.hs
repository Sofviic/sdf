module Matrix4x4 where

import Control.Comonad
import Data.List(transpose)
import qualified Vector4 as V

type Matrix4x4 a = V.Vector4 (V.Vector4 a)

-- eww
matElem :: Int -> Int -> Matrix4x4 a -> a
matElem x y = (case y of
                0 -> V.v4x
                1 -> V.v4y
                2 -> V.v4z
                3 -> V.v4w
                _ -> error $ "Matrix4x4 index out of bounds: " ++ show (x,y)
                ) .
                (case x of
                0 -> V.v4x
                1 -> V.v4y
                2 -> V.v4z
                3 -> V.v4w
                _ -> error $ "Matrix4x4 index out of bounds: " ++ show (x,y)
                )

toList :: Matrix4x4 a -> [[a]]
toList = transpose . V.toList . fmap V.toList
fromList :: [[a]] -> Matrix4x4 a
fromList = V.fromList . fmap V.fromList . transpose

zero, one :: Num a => Matrix4x4 a
zero = (pure . pure) 0
one = V.Vector4 V.x V.y V.z V.w

trans :: Matrix4x4 a -> Matrix4x4 a
trans = fromList . transpose . toList

(|*) :: Num a => Matrix4x4 a -> V.Vector4 a -> V.Vector4 a
m |* v = fmap (V.vsum . (*v)) $ trans m

(|*|) :: Num a => Matrix4x4 a -> Matrix4x4 a -> Matrix4x4 a
a |*| b = fmap (\v -> fmap (V.vsum . (*v)) $ trans a) b

unitTest0 :: Bool
unitTest0 = if a |*| b == c then True else error $ "unitTest0 Failed! " ++ show c
            where a = fromList [[0..3],[4..7],[8..11],[12..15]] :: Matrix4x4 Integer
                  b = fromList [[16..19],[20..23],[24..27],[28..31]]
                  c = fromList [[152,158,164,170]
                                ,[504,526,548,570]
                                ,[856,894,932,970]
                                ,[1208,1262,1316,1370]]

