{-# OPTIONS_GHC -fno-warn-name-shadowing #-}
module Bivector4 where

import Prelude hiding(length)
import Control.Comonad(Comonad,extract,duplicate)
import Utils.Compose
import qualified Vector4 as V

data Bivector4 a = Bivector4 a a a a a a deriving (Show,Eq,Ord)

instance Functor Bivector4 where
    fmap f (Bivector4 xy xz xw yz yw zw) = Bivector4 (f xy) (f xz) (f xw) (f yz) (f yw) (f zw)

instance Applicative Bivector4 where
    pure a = Bivector4 a a a a a a
    liftA2 f (Bivector4 axy axz axw ayz ayw azw) (Bivector4 bxy bxz bxw byz byw bzw) = 
            Bivector4 (f axy bxy) (f axz bxz) (f axw bxw) (f ayz byz) (f ayw byw) (f azw bzw)

bv4xy, bv4xz, bv4xw, bv4yz, bv4yw, bv4zw :: Bivector4 a -> a
bv4xy (Bivector4 xy _ _ _ _ _) = xy
bv4xz (Bivector4 _ xz _ _ _ _) = xz
bv4xw (Bivector4 _ _ xw _ _ _) = xw
bv4yz (Bivector4 _ _ _ yz _ _) = yz
bv4yw (Bivector4 _ _ _ _ yw _) = yw
bv4zw (Bivector4 _ _ _ _ _ zw) = zw

instance Monad Bivector4 where
    (Bivector4 xy xz xw yz yw zw) >>= f = 
        Bivector4 (bv4xy.f $ xy) (bv4xz.f $ xz) (bv4xw.f $ xw) (bv4yz.f $ yz) (bv4yw.f $ yw) (bv4zw.f $ zw) 

instance Comonad Bivector4 where
    extract = bv4xy
    duplicate (Bivector4 xy xz xw yz yw zw) = Bivector4 
                                                (Bivector4 xy xz xw yz yw zw) 
                                                (Bivector4 xz xw yz yw zw xy)
                                                (Bivector4 xw yz yw zw xy xz)
                                                (Bivector4 yz yw zw xy xz xw)
                                                (Bivector4 yw zw xy xz xw yz)
                                                (Bivector4 zw xy xz xw yz yw)

instance Foldable Bivector4 where
    foldr f b (Bivector4 xy xz xw yz yw zw) = (f xy . f xz . f xw . f yz . f yw . f zw) b

-- eww
toList :: Bivector4 a -> [a]
toList (Bivector4 xy xz xw yz yw zw) = [xy,xz,xw,yz,yw,zw]
fromList :: [a] -> Bivector4 a
fromList [xy,xz,xw,yz,yw,zw] = (Bivector4 xy xz xw yz yw zw)
fromList _ = error "non 6 list trying to become a Bivector4"
instance Traversable Bivector4 where
    sequenceA = fmap fromList . sequenceA . toList

instance Num a => Num (Bivector4 a) where
    (+) = liftA2 (+)
    (-) = liftA2 (-)
    (*) = liftA2 (*)
    abs = fmap abs
    signum = fmap signum
    fromInteger = pure . fromInteger

bvsum :: Num a => Bivector4 a -> a
bvsum = foldr (+) 0
bvproduct :: Num a => Bivector4 a -> a
bvproduct = foldr (*) 0

zero, xy, xz, xw, yz, yw, zw :: Num a => Bivector4 a
zero = 0
xy = Bivector4 1 0 0 0 0 0
xz = Bivector4 0 1 0 0 0 0
xw = Bivector4 0 0 1 0 0 0
yz = Bivector4 0 0 0 1 0 0
yw = Bivector4 0 0 0 0 1 0
zw = Bivector4 0 0 0 0 0 1

area² :: Num a => Bivector4 a -> a
area² = bvsum . fmap (^2)
area :: Floating a => Bivector4 a -> a
area = sqrt . area²

dist² :: Num a => Bivector4 a -> Bivector4 a -> a
dist² = area² .#. (-)
dist :: Floating a => Bivector4 a -> Bivector4 a -> a
dist = area .#. (-)

dot :: Num a => Bivector4 a -> Bivector4 a -> a
dot = bvsum .#. (*)

scale :: Num a => a -> Bivector4 a -> Bivector4 a
scale = fmap . (*)

normalise :: Floating a => Bivector4 a -> Bivector4 a
normalise v = scale (recip.area $ v) v

inverse :: Floating a => Bivector4 a -> Bivector4 a
inverse v = scale (recip.area² $ v) v

perpendicular3D :: Num a => V.Vector4 a -> Bivector4 a
perpendicular3D (V.Vector4 x y z _) = Bivector4 z (-y) 0 x 0 0

perpendicularV3D :: (Eq a, Floating a) => Bivector4 a -> V.Vector4 a
perpendicularV3D (Bivector4 0  xz _ yz _ _) = V.scale (sqrt $ xz**2 + yz**2) $ V.Vector4 0     (-yz) (-xz) 0
perpendicularV3D (Bivector4 xy 0  _ yz _ _) = V.scale (sqrt $ xy**2 + yz**2) $ V.Vector4 (-yz) 0     (-xy) 0
perpendicularV3D (Bivector4 xy xz _ _  _ _) = V.scale (sqrt $ xy**2 + xz**2) $ V.Vector4 (xz)  (-xy) 0     0

split3D :: Num a => Bivector4 a -> V.Vector4 a
split3D (Bivector4 xy xz _ yz _ _) = 0 + todo xy xz yz

projectV3D :: Num a => V.Vector4 a -> Bivector4 a -> V.Vector4 a
projectV3D (V.Vector4 x y z w) (Bivector4 xy xz _ yz _ _) = 0 + todo V.Vector4 () () () w undefined x y z xy xz yz

todo :: a
todo = error "TODO"
