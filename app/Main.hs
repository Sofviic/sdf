module Main (main) where

import Graphics.Gloss
import qualified Vector4 as V
import qualified Matrix4x4 as M
import qualified Graphics as G
import qualified SDF
import Utils.Tuple
import GHC.Float
import Data.Function

cameraLocation :: V.Vector4 Double
cameraLocation = V.Vector4 0 0 (-15) 0
cameraRay :: V.Vector4 Double
cameraRay = V.normalise $ V.Vector4 0 0 1 0
fov :: G.FOV
fov = (130, 130)

main :: IO ()
main = do
        putStrLn "SDF: Hello World!"
        if not M.unitTest0 
            then return ()
            else do 
                putStrLn "unitTest0 passed."
                display (InWindow "SDF" (both (*G.screendimfactor) G.screendim) (10, 10)) red
                    $ renderPixels allPixels

pixSize :: Float
pixSize = int2Float G.screendimfactor

pixelise :: G.Colour -> Picture
pixelise = flip color (rectangleSolid pixSize pixSize) . (\(r,g,b) -> makeColorI r g b 255)

translatePixel :: Int -> Int -> Picture -> Picture
translatePixel = on translate $ (pixSize*) . int2Float

allPixels :: [(Int,Int)]
allPixels = [(x,y) | x <- [0..fst G.screendim], y <- [0..snd G.screendim]]

renderPixels :: [(Int,Int)] -> Picture
renderPixels = pictures . fmap renderPixel

renderPixel :: (Int,Int) -> Picture
renderPixel (x,y) = uncurry translate (both ((/2) . (*pixSize) . negate . int2Float) G.screendim)
                    . translatePixel x y
                    . pixelise 
                    $ G.displayPixel mySDF cameraLocation cameraRay fov x y

mySDF :: SDF.SDF Double
mySDF = SDF.fatten 5 $ SDF.line (V.Vector4 0 0 0 1) (V.Vector4 0 1 0 1)
